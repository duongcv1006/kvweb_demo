import "./App.css";
import {
  Box,
  Button,
  Chip,
  Grid,
  Link,
  TextField,
  Typography,
} from "@mui/material";
import BoxTitle from "./components/BoxTitle";
import BoxDomain from "./components/BoxDomain";
import { useContext } from "react";
import ConnectDomainContext from "./ConectStore";
import StepConnectDomain from "./components/StepConnectDomain";

function App() {
  const { step } = useContext(ConnectDomainContext);
  return (
    <div className="App">
      <Box className="bg-box-main">
        <BoxTitle />
        <BoxDomain title={"Tên miền mặc định"}>
          <Grid container spacing={2} alignItems={"center"}>
            <Grid item xs={4}>
              Tên miền
            </Grid>
            <Grid item xs={8}>
              cuahangxx.kiotweb.com
            </Grid>

            <Grid item xs={4}>
              Trạng thái
            </Grid>
            <Grid item xs={8}>
              <Chip label="Đã kết nối" color="success" />
            </Grid>

            <Grid item xs={4}>
              Ngày thêm
            </Grid>
            <Grid item xs={8}>
              16 giờ 52 phút, ngày 27/3/2023
            </Grid>
          </Grid>
        </BoxDomain>
        <BoxDomain title={"Tên miền Tuỳ chỉnh"} isShowPrev={step === 2 ? true : false} isBtnDisconnect={step === 3 ? true : false}>
          <StepConnectDomain />
        </BoxDomain>
      </Box>
    </div>
  );
}

export default App;
