import { createContext, useEffect, useState } from "react";
import { reg } from "./constants/constants";
import { useLocalStorage } from "./hook/useLocalStorage";
const ConnectDomainContext = createContext<any>(null);
type Props = {
  children: string | JSX.Element | JSX.Element[];
};
export const ConnectDomainProvider = ({ children }: Props) => {
  const [savedData, setSavedData, clearLocalStorage] = useLocalStorage("data");
  const [errorMess, setErrorMess] = useState<string>(
    savedData().errorMess || ""
  );
  const [isDisable, setDisable] = useState<boolean>(
    savedData().isDisable || true
  );
  const [step, setStep] = useState<number>(savedData().step || 1);
  const [inputValue, setInputValue] = useState<string | null>(
    savedData().inputValue || null
  );
  const [timerCountDown, setTimerCountDown] = useState("00:10:00");
  const [isConnect, setConnect] = useState<boolean>(
    savedData().isConnect || false
  );
  const [dateUpdateConectDomain, setDateUpdateConnectDomain] = useState<any>(
    savedData().dateUpdateConectDomain || null
  );
  const [isDisconnect, setDisconnect] = useState<boolean | null>(savedData().isDisconnect || null);

  useEffect(() => {
    const newData = {
      errorMess: errorMess,
      isDisable: isDisable,
      step: step,
      inputValue: inputValue,
      isConnect: isConnect,
      dateUpdateConectDomain: dateUpdateConectDomain,
      isDisconnect: isDisconnect,
    };
    setSavedData(newData);
  }, [
    setSavedData,
    errorMess,
    isDisable,
    step,
    inputValue,
    isConnect,
    dateUpdateConectDomain,
    isDisconnect,
  ]);

  const onReset = () => {
    setErrorMess("");
    setDisable(true);
    setStep(1);
    setInputValue(null);
    setTimerCountDown("00:10:00");
    setConnect(false);
    setDateUpdateConnectDomain(null);
    setDisconnect(null);
  };
  useEffect(() => {
    if (inputValue) {
      if (!reg.test(inputValue)) {
        setErrorMess("Tên miền không đúng định dạng");
        setDisable(true);
        return;
      }
      setErrorMess("");
      setDisable(false);
    }
  }, [inputValue]);

  return (
    <ConnectDomainContext.Provider
      value={{
        errorMess,
        isDisable,
        setErrorMess,
        setDisable,
        step,
        setStep,
        inputValue,
        setInputValue,
        timerCountDown,
        setTimerCountDown,
        isConnect,
        setConnect,
        setDateUpdateConnectDomain,
        dateUpdateConectDomain,
        isDisconnect,
        setDisconnect,
        onReset,
      }}
    >
      {children}
    </ConnectDomainContext.Provider>
  );
};

export default ConnectDomainContext;
