import ConnectDomainContext from "../../ConectStore";
import { Chip, Grid, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React, { useContext } from "react";

function StepLast() {
  const { inputValue, dateUpdateConectDomain } =
    useContext(ConnectDomainContext);
  return (
    <Box>
      <Grid container spacing={2} alignItems={"center"}>
        <Grid item xs={4}>
          <Typography fontWeight={"bold"}>Tên miền</Typography>
        </Grid>
        <Grid item xs={8}>
          {inputValue}
        </Grid>
        <Grid item xs={4}>
          Trạng thái
        </Grid>
        <Grid item xs={8}>
          <Chip label="Đã kết nối" color="success" />
        </Grid>
        <Grid item xs={4}>
          Ngày thêm
        </Grid>
        <Grid item xs={8}>
          {dateUpdateConectDomain}
        </Grid>
      </Grid>
    </Box>
  );
}

export default StepLast;
