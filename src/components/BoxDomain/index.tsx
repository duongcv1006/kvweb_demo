import { Box, Button, Menu, MenuItem, Typography } from "@mui/material";
import React, { useContext } from "react";
import DriveFileRenameOutlineIcon from "@mui/icons-material/DriveFileRenameOutline";
import ConnectDomainContext from "../../ConectStore";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
interface Props {
  title: string;
  children: JSX.Element;
  isShowPrev?: boolean;
  isBtnDisconnect?: boolean;
}

function BoxDomain(props: Props) {
  const {
    title,
    children,
    isShowPrev = false,
    isBtnDisconnect = false,
  } = props;
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
    onReset();
  };
  const { setStep, onReset } = useContext(ConnectDomainContext);
  return (
    <Box className="box-domain">
      <Box
        className="box-top-title"
        display={"flex"}
        justifyContent={isShowPrev ? "space-between" : "flex-start"}
      >
        <Typography>{title}</Typography>
        {isShowPrev && (
          <Button
            variant="outlined"
            startIcon={<DriveFileRenameOutlineIcon color={"primary"} />}
            onClick={() => setStep(1)}
          >
            Thay đổi tên miền
          </Button>
        )}
        {isBtnDisconnect && (
          <>
            <Button
              id="basic-button"
              aria-controls={open ? "basic-menu" : undefined}
              aria-haspopup="true"
              aria-expanded={open ? "true" : undefined}
              onClick={handleClick}
              style={{ marginLeft: "auto" }}
            >
              <MoreHorizIcon />
            </Button>
            <Menu
              id="basic-menu"
              anchorEl={anchorEl}
              open={open}
              onClose={handleClose}
              MenuListProps={{
                "aria-labelledby": "basic-button",
              }}
            >
              <MenuItem onClick={handleClose}>Huỷ kết nối</MenuItem>
            </Menu>
          </>
        )}
      </Box>
      <Box className="box-content">{children}</Box>
    </Box>
  );
}

export default BoxDomain;
