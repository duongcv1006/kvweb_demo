import ConnectDomainContext from "../../ConectStore";
import { Box } from "@mui/material";
import React, { useContext, useEffect, useRef } from "react";

function CountDown() {
  const Ref = useRef(null);
  const {
    timerCountDown,
    setTimerCountDown,
    isConnect,
    setConnect,
    setStep,
    setDateUpdateConnectDomain,
  } = useContext(ConnectDomainContext);

  const getTimeRemaining = (e) => {
    const total = Date.parse(e) - Date.parse(new Date());
    const seconds = Math.floor((total / 1000) % 60);
    const minutes = Math.floor((total / 1000 / 60) % 60);
    const hours = Math.floor((total / 1000 / 60 / 60) % 24);
    return {
      total,
      hours,
      minutes,
      seconds,
    };
  };

  const startTimer = (e) => {
    let { total, hours, minutes, seconds } = getTimeRemaining(e);
    if (total >= 0) {
      setTimerCountDown(
        (hours > 9 ? hours : "0" + hours) +
          ":" +
          (minutes > 9 ? minutes : "0" + minutes) +
          ":" +
          (seconds > 9 ? seconds : "0" + seconds)
      );
    }
  };

  const clearTimer = (e) => {
    if (Ref.current) clearInterval(Ref.current);
    const id = setInterval(() => {
      startTimer(e);
    }, 1000);
    Ref.current = id;
  };

  const getDeadTime = () => {
    let deadline = new Date();
    deadline.setMinutes(deadline.getMinutes() + 10);
    return deadline;
  };
  useEffect(() => {
    if (isConnect) {
      clearTimer(getDeadTime());
    }
  }, [isConnect]);

  useEffect(() => {
    if (timerCountDown === "00:00:00") {
      setConnect(false);
      setDateUpdateConnectDomain(formatDate(new Date()));
      setStep(3);
    }
  }, [timerCountDown]);

  function formatDate(date) {
    return [
      padTo2Digits(date.getDate()),
      padTo2Digits(date.getMonth() + 1),
      date.getFullYear(),
    ].join("/");
  }

  function padTo2Digits(num) {
    return num.toString().padStart(2, "0");
  }

  return (
    <Box>
      <h2>{timerCountDown}</h2>
    </Box>
  );
}

export default CountDown;
