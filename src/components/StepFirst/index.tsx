import ConnectDomainContext from "../../ConectStore";
import { Box, Button } from "@mui/material";
import React, { useContext } from "react";
import BoxFormDomain from "../BoxformDomain";

interface Props {}

function StepFirst(props: Props) {
  const {} = props;
  const { isDisable, setStep } = useContext(ConnectDomainContext);

  return (
    <>
      <BoxFormDomain label={"Nhập tên miền của bạn"} />
      <Box display={"flex"} marginTop={5}>
        <Button
          disabled={isDisable}
          style={{ marginLeft: "auto" }}
          variant="contained"
          onClick={() => setStep(2)}
        >
          Tiếp tục
        </Button>
      </Box>
    </>
  );
}

export default StepFirst;
