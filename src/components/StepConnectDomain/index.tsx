import ConnectDomainContext from "../../ConectStore";
import React, { useContext, useEffect } from "react";
import StepFirst from "../StepFirst";
import StepSecond from "../StepSecond";
import StepLast from "../StepLast";

interface Props {}

function StepConnectDomain(props: Props) {
  const {} = props;
  const { step } = useContext(ConnectDomainContext);
  useEffect(() => {
    renderStepConnectDomain();
  }, [step]);
  const renderStepConnectDomain = () => {
    switch (step) {
      case 1:
        return <StepFirst />;
        break;
      case 2:
        return <StepSecond />;
        break;
      case 3:
        return <StepLast />;
        break;
      default:
        return <StepFirst />;
        break;
    }
  };
  return <>{renderStepConnectDomain()}</>;
}

export default StepConnectDomain;
