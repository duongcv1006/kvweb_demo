import ConnectDomainContext from "../../ConectStore";
import {
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  Button,
} from "@mui/material";
import React, { useContext, useState } from "react";
import BoxDomain from "../BoxDomain";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import { Box } from "@mui/system";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import WarningAmberIcon from "@mui/icons-material/WarningAmber";
import CountDown from "../CountDown";
interface Props {}

function StepSecond(props: Props) {
  const {} = props;
  const { inputValue, isConnect, setConnect } =
    useContext(ConnectDomainContext);
  // const [isConnect, setConnect] = useState<boolean>(false);

  const handlerCopyValue = (e: any) => {
    const string: string = "123.456.789";
    navigator.clipboard.writeText(string);
  };
  return (
    <>
      <Grid container spacing={2} alignItems={"center"}>
        {isConnect && (
          <Grid item xs={12}>
            <CountDown />
          </Grid>
        )}
        <Grid item xs={4}>
          <Typography fontWeight={"bold"}>Tên miền</Typography>
        </Grid>
        <Grid item xs={8}>
          {inputValue}
        </Grid>

        <Grid item xs={4}>
          <Typography fontWeight={"bold"}>IP hiện tại</Typography>
        </Grid>
        <Grid item xs={8}>
          <Box display={"flex"} alignItems={"center"}>
            <Typography>123.456.789</Typography>
            <CheckCircleIcon style={{ marginLeft: "3px" }} color={"success"} />
          </Box>
        </Grid>

        <Grid item xs={12}>
          <Box>
            <Typography fontWeight={"bold"}>IP KiotVietWeb:</Typography>
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell align="left">Host</TableCell>
                    <TableCell align="left">Loại</TableCell>
                    <TableCell align="left">Giá trị</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      @
                    </TableCell>
                    <TableCell align="left">A</TableCell>
                    <TableCell align="left">
                      <Box
                        display={"flex"}
                        justifyContent={"space-between"}
                        alignItems={"center"}
                      >
                        <Typography>123.456.789</Typography>
                        <Button
                          startIcon={<ContentCopyIcon color={"primary"} />}
                          onClick={(e) => handlerCopyValue(e)}
                        >
                          Sao chép
                        </Button>
                      </Box>
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
        </Grid>
        {!isConnect && (
          <Grid
            item
            xs={12}
            bgcolor={"#fff5e5"}
            border={"2px solid #ffd99f"}
            borderRadius={"10px"}
            padding={"5px"}
          >
            <Box display={"flex"}>
              <Box marginRight={"5px"}>
                <WarningAmberIcon style={{ color: "#ffd99f" }} />
              </Box>
              <Box>
                <Typography fontSize={"14px"}>
                  <span style={{ fontWeight: "bold" }}>Lưu ý:</span>
                  Nhập đúng địa chỉ IP KVWEB sang nhà cung cấp. Thời gian kết
                  nối IP sẽ tuỳ thuộc vào nhà cung cấp (Khoảng 2-5 tiếng)
                </Typography>
                <Box marginTop={"10px"}>
                  <Typography fontWeight={"bold"}>
                    Hướng dẫn chi tiết từ nhà cung cấp
                  </Typography>
                  <ul style={{ marginTop: "6px" }}>
                    <li>
                      matbao.vn <a href="#">xem chi tiết</a>
                    </li>
                    <li>
                      Couldflare <a href="#">xem chi tiết</a>
                    </li>
                    <li>
                      pavietnam <a href="#">xem chi tiết</a>
                    </li>
                    <li>
                      Nhà cung cấp khác <a href="#">xem chi tiết</a>
                    </li>
                  </ul>
                </Box>
              </Box>
            </Box>
          </Grid>
        )}
        {isConnect && (
          <Grid
            item
            xs={12}
            bgcolor={"#fff5e5"}
            border={"2px solid #ffd99f"}
            borderRadius={"10px"}
            padding={"5px"}
          >
            <Typography>
              Hệ thống đang thiết lập tên miền. Bạn có thể truy cập tên miền sau{" "}
              <span style={{ fontWeight: "bold" }}>10phút</span>, nếu không được
              vui lòng liên hệ hotline{" "}
              <span style={{ fontWeight: "bold" }}>19006522</span> để được hỗ
              trợ.
            </Typography>
          </Grid>
        )}
        {!isConnect && (
          <Grid item xs={12} display={"flex"}>
            <Button
              style={{ marginLeft: "auto" }}
              variant="contained"
              onClick={() => setConnect(true)}
            >
              Kết nối
            </Button>
          </Grid>
        )}
      </Grid>
    </>
  );
}

export default StepSecond;
