import { TextField } from "@mui/material";
import React, { useContext } from "react";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import InputAdornment from "@mui/material/InputAdornment";
import ErrorOutlineIcon from "@mui/icons-material/ErrorOutline";
import ConnectDomainContext from "../../ConectStore";
interface Props {
  label: string;
}
function BoxFormDomain(props: Props) {
  const { errorMess, setInputValue, inputValue } = useContext(ConnectDomainContext);
  const { label } = props;
  return (
    <>
      <form>
        <TextField
          label={label}
          fullWidth
          autoComplete="none"
          name="domain"
          onBlur={(e: any) => setInputValue(e?.target.value)}
          onChange={(e: any) => setInputValue(e?.target.value)}
          value={inputValue}
          helperText={errorMess && errorMess}
          error={errorMess ? true : false}
          InputProps={{
            endAdornment: errorMess && (
              <>
                {!errorMess ? (
                  <InputAdornment position="end">
                    <CheckCircleIcon color={"success"} />
                  </InputAdornment>
                ) : (
                  <InputAdornment position="end">
                    <ErrorOutlineIcon color={"error"} />
                  </InputAdornment>
                )}
              </>
            ),
          }}
        />
      </form>
    </>
  );
}

export default BoxFormDomain;
