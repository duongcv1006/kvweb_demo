import React from "react";
import { Box, Link, Typography } from "@mui/material";
import { grey } from '@mui/material/colors';

interface Props {}

function BoxTitle(props: Props) {
  const {} = props;
  const colorSmallText = grey[500];


  return (
    <Box p={2}>
      <Typography textAlign={'left'} variant="h5">Tên miền</Typography>
      <Box display={"flex"} flexDirection={"column"} alignItems={"flex-start"}>
        <Typography variant="caption" gutterBottom>
          Tăng khả năng hiển thị trang website của bạn
        </Typography>
        <Typography color={colorSmallText} variant="caption">
          Bạn có thể <Link href="#">xem hướng dẫn tại đây</Link>
        </Typography>
      </Box>
    </Box>
  );
}

export default BoxTitle;
